#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <wait.h>

char* link_drive = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
char* nama_zip = "hehe.zip";

void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    printf("Child Spawned\n");
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else {
    wait(&status);
  }
}

int main() {
  char* download_argv[] = { "wget", "--no-check-certificate", link_drive, "-O", nama_zip, NULL };
  bash_command(download_argv, "/bin/wget");
  char* unzip_argv[] = { "unzip", "-n", nama_zip,  NULL };
  bash_command(unzip_argv, "/bin/unzip");
}
