#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define MAX 500

#define ROW 4
#define COL 5

#define SHM_SIZE sizeof(int[ROW][COL])
#define SHM_KEY 1234
#define MAX 500

void printNumber(int *arr, int n) {
  for (int i = 0; i < n; i++) {
    printf("%d", arr[i]);
  }
  printf(" ");
}

int multiply(int x, int *res, int res_size)
{
    int carry = 0;
 
    for (int i = 0; i < res_size; i++) {
        int prod = res[i] * x + carry;
 
        res[i] = prod % 10;
 
        carry = prod / 10;
    }
 
    while (carry) {
        res[res_size] = carry % 10;
        carry = carry / 10;
        res_size++;
    }
    return res_size;
}

void factorial(int n) {
  int *res = malloc(sizeof(int) * MAX);

  res[0] = 1;
  int res_size = 1;

  for (int x = 2; x <= n; x++) {
    res_size = multiply(x, res, res_size);
  }

  int* final_result = calloc(res_size, sizeof(int));
  for (int i = 0; i < res_size; i++) {
    final_result[i] = res[res_size - i - 1];
  }

  printNumber(final_result, res_size);
}

void printMatrix(int* matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", *(matrix + i * cols + j));
        }
        printf("\n");
    }
}

void printFactorialMatrix(int* matrix, int rows, int cols) {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      factorial(*(matrix + i * cols + j));
    }
    printf("\n");
  }
}

int main() {
  int shmid = shmget(SHM_KEY, SHM_SIZE, IPC_CREAT | 0666);
  int *value = shmat(shmid, NULL, 0);

  printf("Matrix in shared memory:\n");
  printMatrix(value, ROW, COL);

  printf("Factorial of values in matrix:\n");
  clock_t start = clock();
  printFactorialMatrix(value, ROW, COL);
  clock_t end = clock();
 
  double time = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("Time elapsed: %f seconds\n", time);

  shmdt(value);
}
