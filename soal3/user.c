#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <stdbool.h>

#define MAX_MSG_SIZE 512

struct msg_buf {
    long msgType;
    char msgText[MAX_MSG_SIZE];
    pid_t pid; 
} msg;

int main() {
    key_t key;
    pid_t pid;
    long msgId;
    char command[MAX_MSG_SIZE];
    key = ftok("songfile", 'R');
    msgId = msgget(key, 0666 | IPC_CREAT);
    pid = getpid();

    while (true) {
        printf("Command: ");
        fgets(command, MAX_MSG_SIZE, stdin);
        command[strcspn(command, "\n")] = '\0';
        msg.pid = pid;
        msg.msgType = 1;
        strcpy(msg.msgText, command);
        msgsnd(msgId, &msg, sizeof(msg), 0);
    }

    return 0;
}