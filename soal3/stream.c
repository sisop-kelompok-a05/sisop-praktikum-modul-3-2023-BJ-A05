#include "../json-c/json.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE    512

const char* PLAYLIST_FILE = "./playlist.txt";
const char* JSON_FILE = "./song-playlist.json";

char user_playlist[1024][1024];

int playlistSize = 0;

int user1 = 1, user2 = 1, userNum = 1;

struct msgbuf {
    long msgType;
    char msgText[1024];
    pid_t pid;
} buffer;

int compare(const void *a, const void *b) {
    char *str1 = *(char **)a;
    char *str2 = *(char **)b;

    char *str1Lower = strdup(str1);
    for (int i = 0; str1Lower[i]; i++) {
        str1Lower[i] = tolower(str1Lower[i]);
    }

    char *str2Lower = strdup(str2);
    for (int i = 0; str2Lower[i]; i++) {
        str2Lower[i] = tolower(str2Lower[i]);
    }

    int res = strcmp(str1Lower, str2Lower);

    free(str1Lower);
    free(str2Lower);

    return res;
}

void decrypt() {
    char *fileContents;
    int sumSong;
    size_t fileSize;

    FILE *JSON = fopen(JSON_FILE, "r");

    if (JSON == NULL) {
        perror("fopen");
        exit(1);
    }

    fseek(JSON, 0, SEEK_END);
    fileSize = ftell(JSON);
    rewind(JSON);

    fileContents = (char *)malloc(sizeof(char) * fileSize);
    fread(fileContents, sizeof(char), fileSize, JSON);
    fclose(JSON);

    struct json_object *json = json_tokener_parse(fileContents);
    free(fileContents);

    if (!json_object_is_type(json, json_type_array)) {
        printf("Invalid JSON file\n");
        json_object_put(json);
        return;
    }

    sumSong = json_object_array_length(json);

    FILE *playlistFile = fopen(PLAYLIST_FILE, "w");

    if (playlistFile == NULL) {
        printf("Can't write playlist.txt file");
        return;
    }

    for (int i = 0; i < sumSong; i++) {
        struct json_object *song = json_object_array_get_idx(json, i);
        struct json_object *encryptMethod;
        struct json_object *songStr;

        if (json_object_object_get_ex(song, "method", &encryptMethod) &&
            json_object_object_get_ex(song, "song", &songStr)) {
                const char* method = json_object_get_string(encryptMethod);
                const char* encodedStr = json_object_get_string(songStr);

                int stringLen = strlen(encodedStr);

                if (strcmp(method, "rot13") == 0) {
                    char *decoded = (char *)malloc(stringLen + 1);

                    for (int j = 0; j < stringLen; j++) {
                        char ch = encodedStr[j];

                        if (ch <= 'z' && ch >= 'a') {
                            decoded[j] = (((ch - 'a') + 13) % 26) + 'a';
                        } else if (ch <= 'Z' && ch >= 'A') {
                            decoded[j] = (((ch - 'A') + 13) % 26) + 'A';
                        } else {
                            decoded[j] = ch;
                        }
                    }

                    decoded[stringLen] = '\0';
                    fprintf(playlistFile, "%s\n", decoded);
                    free(decoded);
                } else if (strcmp(method, "base64") == 0) {
                    const char CHARS[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
                    int outputLen = (stringLen / 4) * 3;
                    
                    if (encodedStr[outputLen - 1] == '=') {
                        outputLen--;

                        if (encodedStr[outputLen - 2] == '=') {
                            outputLen--;
                        }
                    }

                    char *decoded = (char *)malloc(outputLen + 1);

                    int a, b;
                    unsigned char d, e, f, g;
                    for (int a = 0, b = 0; a < stringLen; a += 4, b += 3) {
                        d = strchr(CHARS, encodedStr[a]) - CHARS;
                        e = strchr(CHARS, encodedStr[a + 1]) - CHARS;
                        f = strchr(CHARS, encodedStr[a + 2]) - CHARS;
                        g = strchr(CHARS, encodedStr[a + 3]) - CHARS;

                        decoded[b] = (d << 2) | (e >> 4);

                        if (encodedStr[a + 2] != '=')
                            decoded[b + 1] = (e << 4) | (f >> 2);
                        
                        if (encodedStr[a + 3] != '=')
                            decoded[b + 2] = (f << 6) | g;
                    }

                    decoded[outputLen] = '\0';
                    fprintf(playlistFile, "%s\n", decoded);
                    free(decoded);
                } else if (strcmp(method, "hex") == 0) {
                    char *decoded = (char *)malloc((stringLen / 2) + 1);

                    for (int j = 0; j < stringLen; j += 2) {
                        char bytes[3];
                        bytes[0] = encodedStr[j];
                        bytes[1] = encodedStr[j + 1];
                        bytes[2] = '\0';

                        decoded[j / 2] = strtol(bytes, NULL, 16);
                    }

                    decoded[stringLen / 2] = '\0';
                    fprintf(playlistFile, "%s\n", decoded);
                    free(decoded);
                }
            } 
    }

}

void sortPlaylist() {
    int len = 1024;

    char *rows[len];
    char line[len];

    int count = 0;

    FILE *playlist = fopen(PLAYLIST_FILE, "r");    
    if (playlist == NULL) {
        printf("Playlist file can't be opened\n");
        exit(1);
    }

    while (fgets(line, len, playlist) != NULL) {
        line[strcspn(line, "\n")] = '\0';
        rows[count] = malloc(strlen(line) + 1);
        strcpy(rows[count], line);
        count++;
    }
    fclose(playlist);

    qsort(rows, count, sizeof(char*), compare);

    FILE *sortedPlaylist = fopen(PLAYLIST_FILE, "wb");
    if (sortedPlaylist == NULL) {
        printf("Sorted playlist can't be created\n");
        exit(1);
    }

    for (int i = 0; i < count; i++) {
        fprintf(sortedPlaylist, "%s\n", rows[i]);
        free(rows[i]);
    }
    fclose(sortedPlaylist);
    printf("Playlist has been sorted\n");
}

void listSong() {
    FILE *playlist = fopen(PLAYLIST_FILE, "r");
    if (playlist == NULL) {
        printf("Playlist file can't be opened\n");
        exit(1);
    }

    char line[256];
    printf("List of all songs:\n");
    while (fgets(line, sizeof(line), playlist)) {
        line[strcspn(line, "\n")] = '\0';
        printf("%s\n", line);
    }
    fclose(playlist);
}

void loadPlaylist() {
    char line[1024];
    FILE *playlist = fopen(PLAYLIST_FILE, "r");
    if (playlist == NULL) {
        printf("User playlist can't be opened\n");
        exit(1);
    }

    while (fgets(line, sizeof(line), playlist) != NULL) {
        line[strcspn(line, "\n")] = '\0';

        if (playlistSize < 1024) {
            strncpy(user_playlist[playlistSize], line, sizeof(user_playlist[playlistSize]));
            playlistSize++;
        } else {
            printf("Can't add %s, playlist is full.\n", line);
        }
    }

    fclose(playlist);
}

bool isSongInPlaylist(char *song) {
    bool res = false;
    FILE *playlistFile = fopen(PLAYLIST_FILE, "r");
    if (playlistFile == NULL) {
        printf("Playlist song can't be opened\n");
        exit(1);
    }

    char line[1024];
    while (fgets(line, sizeof(line), playlistFile)) {
        line[strcspn(line, "\n")] = '\0';

        if (strcmp(line, song) == 0) {
            res = true;
            break;
        }
    }
    fclose(playlistFile);

    return res;
}

void addSong(char *song, int userId) {
    FILE *userPlaylist = fopen(PLAYLIST_FILE, "a");
    int sameSong = 0;
    if (userPlaylist == NULL) {
        printf("User playlist can't be opened\n");
        exit(1);
    }

    for (int i = 0; i < playlistSize; i++) {
        if (strcasecmp(user_playlist[i], song) == 0) {
            sameSong = 1;
            break;
        }
    }

    if (sameSong == 1) {
        printf("SONG ALREADY ON PLAYLIST\n");
    } else {
        fprintf(userPlaylist, "%s\n", song);
        fclose(userPlaylist);
        printf("USER <%d> ADD %s\n", userId, song);
    }
}

void playSong(char *song, int userId) {
    int count = 0;
    char upper[1024];
    char matchedSong[1024][1024];

    strncpy(upper, song, sizeof(upper));
    for (int i = 0; i < strlen(upper); i++) {
        upper[i] = toupper(upper[i]);
    }

    for (int i = 0; i < playlistSize; i++) {
        char upperPlaylist[1024];
        strncpy(upperPlaylist, user_playlist[i], sizeof(upperPlaylist));
        
        for (int j = 0; j < strlen(upperPlaylist); j++) {
            upperPlaylist[j] = toupper(upperPlaylist[j]);
        }

        if (strstr(upperPlaylist, upper) != NULL) {
            strncpy(matchedSong[count], user_playlist[i], sizeof(matchedSong[count]));
            count++;
        }
    }

    if (count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song);
    } else if (count == 1) {
        printf("USER <%d> PLAYING \"%s\"\n", userId, matchedSong[0]);
    } else {
        printf("THERE ARE %d SONGS CONTAINING \"%s\":\n", count, song);
        for (int i = 0; i < count; i++) {
            char upperMatched[512];
            strncpy(upperMatched, matchedSong[i], sizeof(upperMatched));
            for (int j = 0; j < strlen(upperMatched); j++) {
                upperMatched[i] = toupper(upperMatched[i]);
            }
            printf("%d. %s\n", i + 1, upperMatched);
        }
    }
}

int main() {
    key_t key;
    sem_t sem;
    key = ftok("songfile", 'R');
    int msgId = msgget(key, 0666 | IPC_CREAT);
    int semId = semget(key, 1, 0666 | IPC_CREAT);
    sem_init(&sem, 0, 2);
    loadPlaylist();

    while (true) {
        msgrcv(msgId, &buffer, sizeof(buffer), 0, 0);
        printf("Message received: %s\n", buffer.msgText);

        userNum = 1;
        sem_wait(&sem);

        if (user1 != 1 && user2 == 2) {
            user2 = buffer.pid;
        }

        if (user1 == 1) {
            user1 = buffer.pid;
            if (user2 == 1) {
                user2++;
            }
        }

        if (user2 == 1 && user1 != 0 && user1 != buffer.pid) {
            user2 = buffer.pid;
        }

        if (user1 == 0 && user2 != buffer.pid) {
            user1 = buffer.pid;
            if (user2 == 0) {
                user2++;
            }
        }

        if (strcmp(buffer.msgText, "EXIT") == 0) {
            if (user1 == buffer.pid) {
                user1 = 1;
                userNum = 0;
            } else if (user2 == buffer.pid) {
                user2 = 2;
                userNum = 0;
            }
        }

        if (user1 != buffer.pid && user2 != buffer.pid && user1 != 1 && user2 > 2) {
            printf("STREAM SYSTEM OVERLOAD\n");
            userNum = 0;
        }

        if (userNum == 1) {
            if (strcmp(buffer.msgText, "DECRYPT") == 0) {
                decrypt();
                sortPlaylist();
            } else if (strcmp(buffer.msgText, "LIST") == 0) {
                listSong();
            } else if (strncmp(buffer.msgText, "PLAY", 4) == 0) {
                char song[1024];
                sscanf(buffer.msgText, "PLAY \"%[^\"]\"", song);
                playSong(song, buffer.pid);
            } else if (strncmp(buffer.msgText, "ADD ", 4) == 0) {
                char song[1024];
                sscanf(buffer.msgText, "ADD %[^\"]", song);
                addSong(song, buffer.pid);
            } else {
                printf("UNKNOWN COMMAND\n");
            }
        }

        sem_post(&sem);
    }

    msgctl(msgId, IPC_RMID, NULL);
    sem_destroy(&sem);

    return 0;
}
