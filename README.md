# sisop-praktikum-modul-3-2023-BJ-A05

## Anggota Kelompok

|     | Nama                      | NRP        |
| --- | ------------------------- | ---------- |
| 1   | Satria Sulthan Sabilillah | 5025201267 |
| 2   | Daud Dhiya' Rozaan        | 5025211021 |
| 3   | Urdhanaka Aptanagi        | 5025211123 |

## Soal 1

Soal ini meminta untuk meng-encode sebuah file txt menggunakan huffman tree. Program yang digunakan dibagi menjadi 2 proses, yaitu proses parent yang berfungsi untuk menghitung frekuensi kemunculan huruf pada file txt awal kemudian mengirim hasil perhitungan ke proses child. Kemudian proses child menerima hasil perhitungan frekuensi dari proses parent dan membuat huffman tree dari perhitungan tersebut sekaligus meng-encode file txt. Kemudian dari proses child mengirim hasil huffman tree yang telah dibuat kembali ke proses parent. Proses parent akan men-decode encoded file yang telah dibuat oleh proses child dan kemudian menghitung perbandingan bit txt awal dan txt yang telah di-encode.

Untuk membuat huffman tree yang digunakan untuk mengkompres file txt yang diberikan, maka diperlukan sebuah struktur data untuk tree yang akan digunakan:

```c
typedef struct node {
    char letter;
    int freq;
    struct node *left, *right;
} Node;

typedef struct heap {
    int size;
    int capacity;
    Node **arr;
} minHeap;

Node* newNode(char letter, int freq) {
    Node *temp = (Node *)malloc(sizeof(Node));

    temp->left = temp->right = NULL;
    temp->letter = letter;
    temp->freq = freq;

    return temp;
}

decodeTree* newDecodeTree() {
    decodeTree *tree = (decodeTree *)malloc(sizeof(decodeTree));

    tree->left = NULL;
    tree->right = NULL;
    tree->letter = '$';

    return tree;
}

minHeap* createMinHeap(int capacity) {
    minHeap *mHeap = (minHeap *)malloc(sizeof(minHeap));

    mHeap->size = 0;
    mHeap->capacity = capacity;
    mHeap->arr = (Node **)malloc(mHeap->capacity * sizeof(Node *));

    return mHeap;
}

void swapNode(Node **a, Node **b) {
    Node *t = *a;
    *a = *b;
    *b = t;
}

void heapify(minHeap *mHeap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < mHeap->size && mHeap->arr[left]->freq < mHeap->arr[smallest]->freq) {
        smallest = left;
    }

    if (right < mHeap->size && mHeap->arr[right]->freq < mHeap->arr[smallest]->freq) {
        smallest = right;
    }

    if (smallest != idx) {
        swapNode(&mHeap->arr[smallest], &mHeap->arr[idx]);
        heapify(mHeap, smallest);
    }
}

int checkSizeOne(minHeap *mHeap) {
    return (mHeap->size == 1);
}

Node* extractMin(minHeap *mHeap) {
    Node *temp = mHeap->arr[0];
    mHeap->arr[0] = mHeap->arr[mHeap->size - 1];

    --mHeap->size;
    heapify(mHeap, 0);

    return temp;
}

void insertMinHeap(minHeap *mHeap, Node *minHeapNode) {
    ++mHeap->size;
    int i = mHeap->size - 1;

    while (i && minHeapNode->freq < mHeap->arr[(i - 1) / 2]->freq) {
        mHeap->arr[i] = mHeap->arr[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    mHeap->arr[i] = minHeapNode;
}

void buildMinHeap(minHeap *mHeap) {
    int n = mHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i) {
        heapify(mHeap, i);
    }
}

int isLeaf(Node *root) {
    return !(root->left) && !(root->right);
}

minHeap* createAndBuildMinHeap(char item[], int freq[], int size) {
    minHeap *mHeap = createMinHeap(size);

    for (int i = 0; i < size; ++i) {
        mHeap->arr[i] = newNode(item[i], freq[i]);
    }

    mHeap->size = size;
    buildMinHeap(mHeap);

    return mHeap;
}

Node* buildHuffmanTree(char item[], int freq[], int size) {
    Node *left, *right, *top;
    minHeap *mHeap = createAndBuildMinHeap(item, freq, size);

    while (!checkSizeOne(mHeap)) {
        left = extractMin(mHeap);
        right = extractMin(mHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(mHeap, top);
    }
    
    return extractMin(mHeap);
}

void saveHuffman(Node *root, char huffmanArr[26][30], int arr[], int n, char ch) {
    if (root -> left) {
        arr[n] = 0;
        saveHuffman(root->left, huffmanArr, arr, n + 1, ch);
    }

    if (root->right) {
        arr[n] = 1;
        saveHuffman(root->right, huffmanArr, arr, n + 1, ch);
    }

    if (isLeaf(root)) {
        if (root->letter == ch) {
            int i;
            for (i = 0; i < n; i++) {
                if (arr[i] == 1 || arr[i] == 0) {
                    huffmanArr[ch - 'A'][i] = arr[i] + '0';
                }

                huffmanArr[ch - 'A'][i + 1] = '\0';
            }
        }
    }
}

void printArray(int arr[], int n, FILE *fp, char ch) {
    int i;
    for (i = 0; i < n; ++i) {
        putc(arr[i] + '0', fp);
    }
}

void printHCodes(Node *root, int arr[], int top, FILE *fp, char ch) {
    if (root->left) {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1, fp, ch);
    }

    if (root->right) {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1, fp, ch);
    }

    if (isLeaf(root)) {
        if (root->letter == ch) {
            printArray(arr, top, fp, ch);
        }
    }
}
```

Referensi: <https://www.programiz.com/dsa/huffman-coding>

Berikut adalah kode yang berisi isi dari main:

```c
    int fd1[2];
    int fd2[2];

    pid_t p;

    if (pipe(fd1) == -1) {
        fprintf(stderr, "Pipe Failed" );
        return 1;
    }

    if (pipe(fd2) == -1) {
        fprintf(stderr, "Pipe Failed" );
        return 1;
    } 

    p = fork();
```

Kode diatas mendeklarasi 2 pipe (`fd1` dan `fd2`) yang akan digunakan untuk mengirim data antara parent dan child process nantinya. Kemudian `pid_t p` digunakan sebagai penanda apakah kode yang dijalankan akan dijalankan oleh parent process atau child process. Untuk kode yang akan dijalankan di parent process:

```c
        int charArr[26] = {0}, charArrToSend[23] = {0}, totalBytes = 0;
        close(fd1[0]);

        // read the file txt
        FILE *fp = fopen("./file.txt", "r");

        // check if txt can be opened
        if (fp == NULL) {
            printf("Can't open the txt file");
            return 1;
        }

        // soal 1a
        char ch;
        while ((ch = fgetc(fp)) != EOF) {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                charArr[toupper(ch) - 'A']++;
            }
        }
        fclose(fp);

        int temp = 0;
        for (int i = 0; i < 26; i++) {
            if (charArr[i] != 0) {
                charArrToSend[temp] = charArr[i];
                temp++;
            }
        }

        write(fd1[1], charArrToSend, sizeof(charArrToSend));
        close(fd1[1]);

        wait(NULL);
```

Kode di atas menghitung frekuensi kemunculan huruf pada file txt yang akan di kompres dan menyimpannya di array `int charArr`. Kemudian karena tidak semua huruf muncul di txt awal, maka `charArrToSend` akan diisi oleh frekuensi huruf-huruf yang muncul di txt. Kemudian `charArrToSend` akan dikirim ke child process dan parent process akan menunggu child process mengirim data ke parent process.

```c
        int temp[MAX_TREE_HT] = {0};
        int charArrTest[23];

        char Char[23] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', \
                        'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'W', 'Y', 'Z'};
        char huffmanArr[30][30];

        close(fd1[1]);

        read(fd1[0], charArrTest, sizeof(charArrTest));
        close(fd1[0]);

        Node *tree = buildHuffmanTree(Char, charArrTest, sizeof(Char) / sizeof(Char[0]));

        FILE *fp = fopen("./file.txt", "r");
        FILE *huffmanFile = fopen("./encoded.txt", "w");

        // check if txt can be opened
        if (fp == NULL) {
            printf("Can't open the txt file");
            return 1;
        }

        // soal 1b
        char ch;
        while ((ch = getc(fp)) != EOF) {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                ch = toupper(ch);
                printHCodes(tree, temp, 0, huffmanFile, ch);
            } else if (ch == ' ') {
                putc(' ', huffmanFile);
            } else if (ch == '\n') {
                putc('\n', huffmanFile);
            }
        }

        for (char ch = 'A'; ch <= 'Z'; ch++) {
            saveHuffman(tree, huffmanArr, temp, 0, ch);
        }

        fclose(fp);
        fclose(huffmanFile);

        write(fd2[1], huffmanArr, sizeof(huffmanArr));
        close(fd2[1]);
        close(fd2[0]);

        exit(0);
```

Kode di atas dijalankan oleh child process ketika telah menerima data dari parent process. Di child process ini akan membuat huffman tree berdasarkan hasil perhitungan frekuensi huruf yang telah dikirim oleh parent process sebelumnya dab kemudian mengkompres file txt awal ke dalam sebuah file baru yang bernama `encoded.txt`. Selain itu child process juga akan menyimpan informasi bit tiap huruf yang akan digunakan untuk men-decode hasil encoding sebelumnya dan mengirimkannya ke parent process untuk men-decode.

```c
        close(fd2[1]);
        FILE *encodedFile = fopen("./encoded.txt", "r");
        FILE *decodedFile = fopen("./decoded.txt", "w");

        char huffmanTree[30][30];

        read(fd2[0], huffmanTree, sizeof(char) * 30 * 30);
        close(fd2[0]);

        // soal 1c
        decode(huffmanTree);

        // soal 1e
        int encodedBits;
        for (int i = 0; i < 26; i++) {
            totalBytes += charArr[i];

            for (int j = 0; j < 30; j++) {
                if (huffmanTree[i][j] == '1' || huffmanTree[i][j] == '0') {
                    encodedBits++;
                } else if (huffmanTree[i][j] == '\0') {
                    j = 30;
                }
            }
        }
        totalBytes *= 8;
        
        printf("Perbandingan bits:\n");
        printf("awal = %d\n", totalBytes);
        printf("encoded = %d\n", encodedBits);

        exit(0);
```

Setelah menerima informasi bit dari tiap huruf, parent process akan men-decode `encoded.txt` dan menyimpannya di `decoded.txt`, kemudian meng-outputkan hasil perbandingan bit sebelum di encode dan setelah di encode.

Hasil encoding:

![Hasil encoding](./img/1/encoding.png)

Hasil decoding:

![Hasil decoding](./img/1/decoding.png)

Perbandingan bit akhir:

![Perbandingan](./img/1/perbandingan.png)

## Soal 2

2.a Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

digunakan thread sebanyak 5, yaitu dimana setiap thread digunakan untuk menghitung hasil perkalian dari setiap kolom matriks, dimana dideklarasikan juga ukuran matriks yang dikalikan, matriks hasil seperti berikut, matriks hasil disini menggunakan pointer karena akan digunakan dalam shared memory. Digunakan juga variabel size dan size1 untuk membantu deklarasi nilai elemen matriks

```c
int arrays1[4][2], arrays2[2][5];
int (*arrays3)[5], size=1, size1=1;``
```

Kemudian ada fungsi multiplier yang merupakan fungsi yg menjalankan tugas masing-masing thread

```c
void *multiplier(void *arg) {
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0]))
    {
        for (int i=0; i<4; i++) {
            arrays3[i][4] = (arrays1[i][0]*arrays2[0][4]) + (arrays1[i][1]*arrays2[1][4]);
        }
    }
    else if(pthread_equal(id, tid[1]))
    {
        for (int i=0; i<4; i++) {
            arrays3[i][3] = (arrays1[i][0]*arrays2[0][3]) + (arrays1[i][1]*arrays2[1][3]);
        }
    }
    else if(pthread_equal(id, tid[2]))
    {
        for (int i=0; i<4; i++) {
            arrays3[i][2] = (arrays1[i][0]*arrays2[0][2]) + (arrays1[i][1]*arrays2[1][2]);
        }
    }
    else if(pthread_equal(id, tid[3]))
    {
        for (int i=0; i<4; i++) {
            arrays3[i][1] = (arrays1[i][0]*arrays2[0][1]) + (arrays1[i][1]*arrays2[1][1]);    
        }
    }
    else 
    {
        for (int i=0; i<4; i++) {
            arrays3[i][0] = (arrays1[i][0]*arrays2[0][0]) + (arrays1[i][1]*arrays2[1][0]);
        }
    }
}
```

Dari setiap id thread dicek untuk menjalankan tugasnya yaitu menghitung dan menyimpan nilai hasil perkalian dari setiap kolom. Untuk perkaliannya, seperti matriks pada umumnya yaitu misal untuk menghitung nilai matriks hasil pada elemen (1,1) maka caranya elemen (1,1) Matriks 1 * elemen (1,1) Matriks 2 + elemen (1,2) Matriks 1 * elemen (2,1) Matriks 2

```c
key_t key = 1234;
int shmid = shmget(key,sizeof(int[4][5]),0666|IPC_CREAT); 
arrays3 =  shmat(shmid,NULL,0);  
```

Kode diatas yaitu digunakan untuk deklarasi shared memory yang akan digunakan untuk mendapatkan hasil dari array perkalian matriks untuk ditampilkan di nomor selanjutnya. key digunakan sebagai parameter dalam shmget. shmget sendiri akan mereturn identifier untuk shared memory, int[4][5] dalam parameter digunakan sebagai acuan ukuran variabel yang ada di shared memory (array hasil perkalian matriks). Dan setelah identifier shared memory didapat,variabel diattach ke shared memory identifier tadi dengan menggunakan shmat.

```c
 printf("Matriks 1:\n");
    srand(time(NULL));
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<2 ;j++)
        {
            arrays1[i][j] = size;
            printf("%d ", arrays1[i][j] = rand() % 5 + 1);
            size++;
        }
        printf("\n");
    }
    printf("\nMatriks2:\n");
    srand(time(NULL));
    for(int i=0; i<2; i++)
    {
        for(int j=0; j<5 ;j++){
            arrays2[i][j] = size1;
            printf("%d ", arrays2[i][j] = rand() % 4 + 1);
            size1++;
        }
        printf("\n");
    }
    printf("\n");
```

Kode tersebut untuk mencetak matriks yang dikalikan.

```c
 while(k<5)
   {
       err = err=pthread_create(&(tid[k]), NULL, &multiplier, NULL); //pembuatan thread
       if(err != 0){
           printf("Can't create thread : [%s]\n", strerror(err));
       }else{
           //printf("Crate thread success\n");
       }
       k++;
   }
   pthread_join(tid[0], NULL);
   pthread_join(tid[1], NULL);
   pthread_join(tid[2], NULL);
   pthread_join(tid[3], NULL);
   pthread_join(tid[4], NULL);
```

Kemudian untuk kode diatas, yaitu untuk menjalankan thread dimana menggunakan perulangan while sejumlah (banyak thread - 1) serta fungsi multiplier yang sudah dibuat sebelumnya. Setelah dijalankan, maka semua thread dijoin dengan perintah pthread_join

```c
printf("Matriks hasil:\n");
    for(int i=0; i<4; i++){
        for(int k=0; k<5; k++)
        {
            printf("%d ", arrays3[i][k]);
        }
        printf("\n");
    }
```

Kode tersebut digunakan untuk mencetak matriks hasil perkalian. Kemudian kode berikut, yaitu shmdt digunakan untuk mendetach variabel dari shared memory. dan exit(0) digunakan untuk keluar dari program

```c
shmdt(arrays3);
exit(0);
```

2.b dan c. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

digunakan thread sebanyak 5, yaitu dimana setiap thread digunakan untuk menghitung hasil penjumlahan dari 1 hingga bilangan di elemen matriks itu sendiri untuk setiap kolom matriks, dimana dideklarasikan juga ukuran matriks yang dikalikan, matriks hasil seperti berikut, matriks hasil disini menggunakan pointer karena akan digunakan dalam shared memory. Digunakan juga variabel size dan size1 untuk membantu deklarasi nilai elemen matriks

```c
int arrays1[4][2], arrays2[2][5];
int (*arrays3)[5], size=1, size1=1;``
```

Kemudian ada fungsi multiplier yang merupakan fungsi yg menjalankan tugas masing-masing thread

```c
void *multiplier(void *arg) 
{
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0]))
    {
        for (int i=0; i<4; i++) 
        {
             for(int j=1; j<=(int)arrays3[i][4]; j++)
             {
                 arrayshasil[i][4]+=j;
             }
        }
    }
    else if(pthread_equal(id, tid[1]))
    {
        for (int i=0; i<4; i++) 
        {
             for(int j=1; j<=(int)arrays3[i][3]; j++)
             {
                 arrayshasil[i][3]+=j;
             }
        }
    }
    else if(pthread_equal(id, tid[2]))
    {
        for (int i=0; i<4; i++) 
        {
             for(int j=1; j<=(int)arrays3[i][2]; j++)
             {
                 arrayshasil[i][2]+=j;
             }
        }
    }
    else if(pthread_equal(id, tid[3]))
    {
        for (int i=0; i<4; i++) 
        {
             for(int j=1; j<=(int)arrays3[i][1]; j++)
             {
                 arrayshasil[i][1]+=j;
             }
        }
    }
    else 
    {
        for (int i=0; i<4; i++) 
        {
             for(int j=1; j<=(int)arrays3[i][0]; j++)
             {
                 arrayshasil[i][0]+=j;
             }
        }
    }
}
```

Dari setiap id thread dicek untuk menjalankan tugasnya yaitu menghitung dan menyimpan nilai hasil penjumlahan dari 1 hingga bilangan di elemen matriks itu sendiri dari setiap kolom. Untuk perhitungannya, seperti matriks pada umumnya yaitu misal untuk menghitung nilai matriks hasil pada elemen (1,1) maka caranya dilakukan perulangan mulai dari 1 hingga bilangan di elemen (1,1) matriks hasil sebelumnya dan ditambahkan elemen matriks hasil baru dengan nilai yang menjadi acuan perulangan.

```c
key_t key = 1234;
int shmid = shmget(key,sizeof(int[4][5]),0666|IPC_CREAT); 
arrays3 =  shmat(shmid,NULL,0);  
```

Kode diatas yaitu digunakan untuk deklarasi shared memory yang akan digunakan untuk mendapatkan hasil dari array perkalian matriks untuk ditampilkan di nomor selanjutnya. key digunakan sebagai parameter dalam shmget. shmget sendiri akan mereturn identifier untuk shared memory, int[4][5] dalam parameter digunakan sebagai acuan ukuran variabel yang ada di shared memory (array hasil perkalian matriks). Dan setelah identifier shared memory didapat,variabel diattach ke shared memory identifier tadi dengan menggunakan shmat.

```c
printf("Matriks hasil:\n");
    for(int i=0; i<4; i++){
        for(int k=0; k<5; k++)
        {
            printf("%d ", arrays3[i][k]);
        }
        printf("\n");
    }
```

Kode tersebut untuk mencetak matriks yang hasil dari perkalian di nomor sebelumnya.

```c
 while(k<5)
   {
       err = err=pthread_create(&(tid[k]), NULL, &multiplier, NULL); //pembuatan thread
       if(err != 0){
           printf("Can't create thread : [%s]\n", strerror(err));
       }else{
           //printf("Crate thread success\n");
       }
       k++;
   }
   pthread_join(tid[0], NULL);
   pthread_join(tid[1], NULL);
   pthread_join(tid[2], NULL);
   pthread_join(tid[3], NULL);
   pthread_join(tid[4], NULL);
```

Kemudian untuk kode diatas, yaitu untuk menjalankan thread dimana menggunakan perulangan while sejumlah (banyak thread - 1) serta fungsi multiplier yang sudah dibuat sebelumnya. Setelah dijalankan, maka semua thread dijoin dengan perintah pthread_join

```c
    printf("\nMatriks Penjumlahan:\n");
    for(int i=0; i<4; i++){
        for(int k=0; k<5; k++)
        {
            printf("%d ", arrayshasil[i][k]);
        }
        printf("\n");
    }
```

Kode tersebut digunakan untuk mencetak matriks hasil penjumlahan. Kemudian kode berikut, yaitu shmdt digunakan untuk mendetach variabel dari shared memory. shmctl digunakan untuk mendestroy shared memory yang dibuat dan exit(0) digunakan untuk keluar dari program

```c
shmdt(arrays3);
shmctl(shmid,IPC_RMID,NULL); 
exit(0);
```
2.d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 

sc 2.d
```c
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define MAX 500

#define ROW 4
#define COL 5

#define SHM_SIZE sizeof(int[ROW][COL])
#define SHM_KEY 1234
#define MAX 500

void printNumber(int *arr, int n) {
  for (int i = 0; i < n; i++) {
    printf("%d", arr[i]);
  }
  printf(" ");
}

int multiply(int x, int *res, int res_size)
{
    int carry = 0;
 
    for (int i = 0; i < res_size; i++) {
        int prod = res[i] * x + carry;
 
        res[i] = prod % 10;
 
        carry = prod / 10;
    }
 
    while (carry) {
        res[res_size] = carry % 10;
        carry = carry / 10;
        res_size++;
    }
    return res_size;
}

void factorial(int n) {
  int *res = malloc(sizeof(int) * MAX);

  res[0] = 1;
  int res_size = 1;

  for (int x = 2; x <= n; x++) {
    res_size = multiply(x, res, res_size);
  }

  int* final_result = calloc(res_size, sizeof(int));
  for (int i = 0; i < res_size; i++) {
    final_result[i] = res[res_size - i - 1];
  }

  printNumber(final_result, res_size);
}

void printMatrix(int* matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", *(matrix + i * cols + j));
        }
        printf("\n");
    }
}

void printFactorialMatrix(int* matrix, int rows, int cols) {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      factorial(*(matrix + i * cols + j));
    }
    printf("\n");
  }
}

int main() {
  int shmid = shmget(SHM_KEY, SHM_SIZE, IPC_CREAT | 0666);
  int *value = shmat(shmid, NULL, 0);

  printf("Matrix in shared memory:\n");
  printMatrix(value, ROW, COL);

  printf("Factorial of values in matrix:\n");
  clock_t start = clock();
  printFactorialMatrix(value, ROW, COL);
  clock_t end = clock();
 
  double time = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("Time elapsed: %f seconds\n", time);

  shmdt(value);
}
```

disini kita hanya membuat instruksi yang sama seperti soal sebelumnya namun tanpa thread dan multithreading\
hasil yang didapatkan oleh kalian.c = `0.000002 seconds`
hasil yang didapatkan oleh cinta.c = `0.000002 seconds`
hasil yang didapatkan oleh sisop.c = `0.000003 seconds`

dapat disimulkan bahwa waktu yang dihasilkan oleh thread dan multithreading kebih cepat daripada yang tidak pakai
## Soal 3

Soal ini meminta untuk menggunakan message queue sebagai bentuk komunikasi antara 2 buah proses, yaitu proses dari `stream.c` dan dari `user.c`. `user.c` akan mengirimkan command yang akan dieksekusi oleh `stream.c`.

Untuk kode dari `user.c` adalah sebagai berikut:

```c
struct msg_buf {
    long msgType;
    char msgText[MAX_MSG_SIZE];
    pid_t pid; 
} msg;

int main() {
    key_t key;
    pid_t pid;
    long msgId;
    char command[MAX_MSG_SIZE];
    key = ftok("songfile", 'R');
    msgId = msgget(key, 0666 | IPC_CREAT);
    pid = getpid();

    while (true) {
        printf("Command: ");
        fgets(command, MAX_MSG_SIZE, stdin);
        command[strcspn(command, "\n")] = '\0';
        msg.pid = pid;
        msg.msgType = 1;
        strcpy(msg.msgText, command);
        msgsnd(msgId, &msg, sizeof(msg), 0);
    }

    return 0;
}
```

`struct msg_buf` digunakan untuk sebagai struktur data pesan yang akan dikirimkan. Kemudian di fungsi main berisi variabel-variabel yang akan digunakan pada saat mengirimkan pesan menggunakan message queue nanti.

Untuk kode dari `stream.c` adalah sebagai berikut:

```c
struct msgbuf {
    long msgType;
    char msgText[1024];
    pid_t pid;
} buffer;
```

Sama seperti di `user.c`, struktur data di atas digunakan untuk menerima pesan dengan struktur data yang sama dari `user.c`.

```c
int compare(const void *a, const void *b) {
    char *str1 = *(char **)a;
    char *str2 = *(char **)b;

    char *str1Lower = strdup(str1);
    for (int i = 0; str1Lower[i]; i++) {
        str1Lower[i] = tolower(str1Lower[i]);
    }

    char *str2Lower = strdup(str2);
    for (int i = 0; str2Lower[i]; i++) {
        str2Lower[i] = tolower(str2Lower[i]);
    }

    int res = strcmp(str1Lower, str2Lower);

    free(str1Lower);
    free(str2Lower);

    return res;
}
```

Function di atas digunakan untuk pada saat mengurutkan string nantinya.

```c
void decrypt() {
    char *fileContents;
    int sumSong;
    size_t fileSize;

    FILE *JSON = fopen(JSON_FILE, "r");

    if (JSON == NULL) {
        perror("fopen");
        exit(1);
    }

    fseek(JSON, 0, SEEK_END);
    fileSize = ftell(JSON);
    rewind(JSON);

    fileContents = (char *)malloc(sizeof(char) * fileSize);
    fread(fileContents, sizeof(char), fileSize, JSON);
    fclose(JSON);

    struct json_object *json = json_tokener_parse(fileContents);
    free(fileContents);

    if (!json_object_is_type(json, json_type_array)) {
        printf("Invalid JSON file\n");
        json_object_put(json);
        return;
    }

    sumSong = json_object_array_length(json);

    FILE *playlistFile = fopen(PLAYLIST_FILE, "w");

    if (playlistFile == NULL) {
        printf("Can't write playlist.txt file");
        return;
    }

    for (int i = 0; i < sumSong; i++) {
        struct json_object *song = json_object_array_get_idx(json, i);
        struct json_object *encryptMethod;
        struct json_object *songStr;

        if (json_object_object_get_ex(song, "method", &encryptMethod) &&
            json_object_object_get_ex(song, "song", &songStr)) {
                const char* method = json_object_get_string(encryptMethod);
                const char* encodedStr = json_object_get_string(songStr);

                int stringLen = strlen(encodedStr);

                if (strcmp(method, "rot13") == 0) {
                    char *decoded = (char *)malloc(stringLen + 1);

                    for (int j = 0; j < stringLen; j++) {
                        char ch = encodedStr[j];

                        if (ch <= 'z' && ch >= 'a') {
                            decoded[j] = (((ch - 'a') + 13) % 26) + 'a';
                        } else if (ch <= 'Z' && ch >= 'A') {
                            decoded[j] = (((ch - 'A') + 13) % 26) + 'A';
                        } else {
                            decoded[j] = ch;
                        }
                    }

                    decoded[stringLen] = '\0';
                    fprintf(playlistFile, "%s\n", decoded);
                    free(decoded);
                } else if (strcmp(method, "base64") == 0) {
                    const char CHARS[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
                    int outputLen = (stringLen / 4) * 3;
                    
                    if (encodedStr[outputLen - 1] == '=') {
                        outputLen--;

                        if (encodedStr[outputLen - 2] == '=') {
                            outputLen--;
                        }
                    }

                    char *decoded = (char *)malloc(outputLen + 1);

                    int a, b;
                    unsigned char d, e, f, g;
                    for (int a = 0, b = 0; a < stringLen; a += 4, b += 3) {
                        d = strchr(CHARS, encodedStr[a]) - CHARS;
                        e = strchr(CHARS, encodedStr[a + 1]) - CHARS;
                        f = strchr(CHARS, encodedStr[a + 2]) - CHARS;
                        g = strchr(CHARS, encodedStr[a + 3]) - CHARS;

                        decoded[b] = (d << 2) | (e >> 4);

                        if (encodedStr[a + 2] != '=')
                            decoded[b + 1] = (e << 4) | (f >> 2);
                        
                        if (encodedStr[a + 3] != '=')
                            decoded[b + 2] = (f << 6) | g;
                    }

                    decoded[outputLen] = '\0';
                    fprintf(playlistFile, "%s\n", decoded);
                    free(decoded);
                } else if (strcmp(method, "hex") == 0) {
                    char *decoded = (char *)malloc((stringLen / 2) + 1);

                    for (int j = 0; j < stringLen; j += 2) {
                        char bytes[3];
                        bytes[0] = encodedStr[j];
                        bytes[1] = encodedStr[j + 1];
                        bytes[2] = '\0';

                        decoded[j / 2] = strtol(bytes, NULL, 16);
                    }

                    decoded[stringLen / 2] = '\0';
                    fprintf(playlistFile, "%s\n", decoded);
                    free(decoded);
                }
            } 
    }

}
```

Function di atas akan men-decrypt pesan rahasia. Selain itu di function ini juga menggunakan library dari `json-c` untuk meng-handle parsing json.

```c
void sortPlaylist() {
    int len = 1024;

    char *rows[len];
    char line[len];

    int count = 0;

    FILE *playlist = fopen(PLAYLIST_FILE, "r");    
    if (playlist == NULL) {
        printf("Playlist file can't be opened\n");
        exit(1);
    }

    while (fgets(line, len, playlist) != NULL) {
        line[strcspn(line, "\n")] = '\0';
        rows[count] = malloc(strlen(line) + 1);
        strcpy(rows[count], line);
        count++;
    }
    fclose(playlist);

    qsort(rows, count, sizeof(char*), compare);

    FILE *sortedPlaylist = fopen(PLAYLIST_FILE, "wb");
    if (sortedPlaylist == NULL) {
        printf("Sorted playlist can't be created\n");
        exit(1);
    }

    for (int i = 0; i < count; i++) {
        fprintf(sortedPlaylist, "%s\n", rows[i]);
        free(rows[i]);
    }
    fclose(sortedPlaylist);
    printf("Playlist has been sorted\n");
}
```

Function di atas akan mengurutkan lagu-lagu yang telah di-decrypt oleh function `decrypt()` sebelumnya.

```c
void listSong() {
    FILE *playlist = fopen(PLAYLIST_FILE, "r");
    if (playlist == NULL) {
        printf("Playlist file can't be opened\n");
        exit(1);
    }

    char line[256];
    printf("List of all songs:\n");
    while (fgets(line, sizeof(line), playlist)) {
        line[strcspn(line, "\n")] = '\0';
        printf("%s\n", line);
    }
    fclose(playlist);
}
```

Function di atas akan meng-outputkan semua lagu yang telah di decrypt dan di sort sebelumnya.

```c
void addSong(char *song, int userId) {
    FILE *userPlaylist = fopen(PLAYLIST_FILE, "a");
    int sameSong = 0;
    if (userPlaylist == NULL) {
        printf("User playlist can't be opened\n");
        exit(1);
    }

    for (int i = 0; i < playlistSize; i++) {
        if (strcasecmp(user_playlist[i], song) == 0) {
            sameSong = 1;
            break;
        }
    }

    if (sameSong == 1) {
        printf("SONG ALREADY ON PLAYLIST\n");
    } else {
        fprintf(userPlaylist, "%s\n", song);
        fclose(userPlaylist);
        printf("USER <%d> ADD %s\n", userId, song);
    }
}
```

Function di atas akan berguna ketika command yang diberikan bertujuan untuk menambahkan lagu ke playlist.

```c
void playSong(char *song, int userId) {
    int count = 0;
    char upper[1024];
    char matchedSong[1024][1024];

    strncpy(upper, song, sizeof(upper));
    for (int i = 0; i < strlen(upper); i++) {
        upper[i] = toupper(upper[i]);
    }

    for (int i = 0; i < playlistSize; i++) {
        char upperPlaylist[1024];
        strncpy(upperPlaylist, user_playlist[i], sizeof(upperPlaylist));
        
        for (int j = 0; j < strlen(upperPlaylist); j++) {
            upperPlaylist[j] = toupper(upperPlaylist[j]);
        }

        if (strstr(upperPlaylist, upper) != NULL) {
            strncpy(matchedSong[count], user_playlist[i], sizeof(matchedSong[count]));
            count++;
        }
    }

    if (count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song);
    } else if (count == 1) {
        printf("USER <%d> PLAYING \"%s\"\n", userId, matchedSong[0]);
    } else {
        printf("THERE ARE %d SONGS CONTAINING \"%s\":\n", count, song);
        for (int i = 0; i < count; i++) {
            char upperMatched[512];
            strncpy(upperMatched, matchedSong[i], sizeof(upperMatched));
            for (int j = 0; j < strlen(upperMatched); j++) {
                upperMatched[i] = toupper(upperMatched[i]);
            }
            printf("%d. %s\n", i + 1, upperMatched);
        }
    }
}
```

Function di atas akan meng-outputkan lagu yang ingin dimainkan oleh user yang mengirimkan command.

```c
int main() {
    key_t key;
    sem_t sem;
    key = ftok("songfile", 'R');
    int msgId = msgget(key, 0666 | IPC_CREAT);
    int semId = semget(key, 1, 0666 | IPC_CREAT);
    sem_init(&sem, 0, 2);
    loadPlaylist();

    while (true) {
        msgrcv(msgId, &buffer, sizeof(buffer), 0, 0);
        printf("Message received: %s\n", buffer.msgText);

        userNum = 1;
        sem_wait(&sem);

        if (user1 != 1 && user2 == 2) {
            user2 = buffer.pid;
        }

        if (user1 == 1) {
            user1 = buffer.pid;
            if (user2 == 1) {
                user2++;
            }
        }

        if (user2 == 1 && user1 != 0 && user1 != buffer.pid) {
            user2 = buffer.pid;
        }

        if (user1 == 0 && user2 != buffer.pid) {
            user1 = buffer.pid;
            if (user2 == 0) {
                user2++;
            }
        }

        if (strcmp(buffer.msgText, "EXIT") == 0) {
            if (user1 == buffer.pid) {
                user1 = 1;
                userNum = 0;
            } else if (user2 == buffer.pid) {
                user2 = 2;
                userNum = 0;
            }
        }

        if (user1 != buffer.pid && user2 != buffer.pid && user1 != 1 && user2 > 2) {
            printf("STREAM SYSTEM OVERLOAD\n");
            userNum = 0;
        }

        if (userNum == 1) {
            if (strcmp(buffer.msgText, "DECRYPT") == 0) {
                decrypt();
                sortPlaylist();
            } else if (strcmp(buffer.msgText, "LIST") == 0) {
                listSong();
            } else if (strncmp(buffer.msgText, "PLAY", 4) == 0) {
                char song[1024];
                sscanf(buffer.msgText, "PLAY \"%[^\"]\"", song);
                playSong(song, buffer.pid);
            } else if (strncmp(buffer.msgText, "ADD ", 4) == 0) {
                char song[1024];
                sscanf(buffer.msgText, "ADD %[^\"]", song);
                addSong(song, buffer.pid);
            } else {
                printf("UNKNOWN COMMAND\n");
            }
        }

        sem_post(&sem);
    }

    msgctl(msgId, IPC_RMID, NULL);
    sem_destroy(&sem);

    return 0;
}
```

Function di atas menjalankan fungsi utama `stream.c`. Function ini memeriksa apakah ketika user yang mengakses lebih dari 2 atau tidak. Selain itu function ini memeriksa command yang dikirimkan oleh user dan menjalankan command yang telah dikirimkan.

function LIST:

![list](./img/3/LIST.png)

function PLAY:

![play](./img/3/PLAY.png)

![play_list](./img/3/PLAY_LIST.png)

![play_no_song](./img/3/PLAY_NO_SONG.png)

## Soal 4

- ### unzip.c

  Mendownload file `hehe.zip` menggunakan command `wget` dan meng-_extract_ nya mengguanakn command `unzip` yang semua argumen tersebut akan dipassing dan diexecute oleh fungsi `bash_command` yang terdapat pada file `unzip.c`

- ### categorize.c

  Mengkategorikan setiap file sesuai extensionnya

  1. Pertama-tama, kita inisialisasi beberapa global variable & struct yang akan dibutuhkan

     ```c
     #define banyakExtension 7
     #define panjangExtension 4
     #define maxFile 10

     typedef struct {
       char name[5];
       int num;
     } ExtensionDetail;

     char tmpMaxFile[panjangExtension], extensions[banyakExtension][panjangExtension];
     ExtensionDetail extensionEntry[banyakExtension + 1];
     ```

     Keterangan :

     - `banyakExtension` : banyak extension yang harus dikategorikan
     - `panjangExtension` : panjang maksimal tiap extension
     - `maxFile` : jumlah maximum pada suatu directory
     - struct `ExtensionDetail` : struct untuk menyimpan masing-masing nama extension beserta jumlah filenya, agar mudah saat sorting nantinya
     - `tmpMaxFile` : buffer variable untuk menyimpan sementara nama-nama extensions saat membaca file `extensions.txt`
     - `extensions` : tempat untuk menyimpan nama-nama extensions

  2. Selanjutnya, kita mengambil extension apa saja yang harus dikategorikan pada file extensions.txt dan selanjutnya akan dimasukkan ke sebuah `array` dengan nama `extensions`

     ```c
     // GET EXTENSIONS
     int idx = 0;
     FILE* ptr = fopen("extensions.txt", "r");
     while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
       strcpy(extensions[idx], tmpMaxFile);
       idx++;
     }
     fclose(ptr);
     ```

  3. Membuat thread untuk tiap masing-masing extension

     ```c
     pthread_t tid[banyakExtension];
     ```

  4. Start program untuk setiap thread untuk mengkategorikan sesuai extensionnya, dengan cara memanggil fungsi `starting_program` dengan argumen yang dipassing adalah nama extension nya (a.k.a `extensions[i]`)

     ```c
     pthread_create(&tid[i], NULL, starting_program, extensions[i]);
     ```

  5. Memberikan `id` untuk setiap thread pada fungsi `starting_program` (karena jika tidak menggunakan ini, `id` nya terkadang random yang menyebabkan ada extension yang tidak terkategorikan oleh thread ini)

     ```c
     char* extensionName = (char*)arg;

     if (strcmp(extensionName, "jpg") == 0) id_thread = 1;
     else if (strcmp(extensionName, "txt") == 0) id_thread = 2;
     else if (strcmp(extensionName, "js") == 0) id_thread = 3;
     else if (strcmp(extensionName, "py") == 0) id_thread = 4;
     else if (strcmp(extensionName, "png") == 0) id_thread = 5;
     else if (strcmp(extensionName, "emc") == 0) id_thread = 6;
     else if (strcmp(extensionName, "xyz") == 0) id_thread = 7;
     ```

     Contoh eror jika tidak menggunakan potongan kode diatas :

     <img width="180" src="img/4/4eror_IDThread.png"/>

  <br/>

  6. Get Number of Files every extension

     ```c
     int numFilePerExtension = getNumFiles(extensionName);
     ```

     fungsi `getNumFiles()` adalah fungsi untuk mendapatkan jumlah file setiap extension menggunakan command `find | wc`

  7. Mengkategorikan setiap 10 file pertama setiap extensionnya dengan cara melooping setiap extension sebanyak :

     **Jumlah File Per Extension / Jumlah Maximum File dalam 1 Folder**

     Jika terdapat lebih dari 10 file untuk 1 extension, makan akan dibuat folder dengan format `extension (ke-n)`

     ```c
     for (int j = 0; j <= numFilePerExtension / maxFile; j++){
        /* */
     }
     ```

  8. Membuat directory untuk setiap 10 file pertama setiap extension dengan format yang telah ditentukan, lalu menuliskan log bahwa telah membuat suatu directiry tersebut. Pada pertama kali, tuliskan juga log `MADE categorized`

     ```c
     if (!(id_thread - 1)) write_log("MADE", "categorized");

     char* mkdir_argv[] = { "mkdir", "-p", bufDirectory, NULL };
     bash_command(mkdir_argv, "/bin/mkdir");
     write_log("MADE", bufDirectory);
     ```

  9. Men-_traverse_ directory `files` untuk setiap 10 file pertama setiap extension menggunakan fungsi `traverse_directory()`. Fungsi ini menggunakan library `dirent.h` untuk mengakses setiap file dan directory yang ada di sebuah folder

     ```c
     struct dirent* entry;

     DIR* dir = opendir(dir_path);
     if (dir == NULL) {
       perror("Error");
       exit(1);
     }
     ```

  10. Setiap akses file maupun directory, tuliskan lognya

      ```c
      write_log("ACCESSED", sub_path);
      ```

  11. Jika terdapat extension yang match/cocok, maka move file tersebut ke folder yang sesuai

      ```c
      move_file(lowerbufExtension, sub_path, bufDirectory);
      ```

      Disini kita menggunakan bantuan fungsi `move_file()` yang di dalamnya menggunakan command `mv` untuk memindahkan seuatu file, yang selanjutnya akan menuliskan log bahwa telah memindahkan suatu file tersebut

      ```c
      write_log("MOVED", new_path);
      ```

  12. Setelah itu, menghitung jumlah file tiap extension kembali karena, walaupun sudah dihitung pada fungsi `starting_program()` tadi, ada kemungkinan kesalahan indexing jumlah file pada struct yang telah diinisialisasi karena thread tersebut berjalan bersaut-sautan atau tidak menunggu satu sama lain. Setelah mendapatkan jumlah file tiap extensionnya, maka akan dimasukkan ke struct yang tadi

      ```c
      int numFilePerExtension = getNumFiles(extensions[i]);
      extensionEntry[i].num = numFilePerExtension;
      ```

  13. Setelah setiap extension dikategorikan menggunakan thread, maka setiap thread tersebut akan di-joinkan

      ```c
      for (int i = 0; i < banyakExtension; i++)
        pthread_join(tid[i], NULL);
      ```

  14. Selanjutnya adalah mengkategorikan file `others` yaitu file yang memiliki extension selain yang ada di file `extensions.txt` tadi. Alurnya kurang lebih sama seperti diatas, akan tetapi bedanya kali ini tidak menggunakan thread saja

      ```c
      // CATEGORIZING OTHERS
      int numOtherFiles = getNumFiles("others");
      strcpy(extensionEntry[7].name, "others");
      extensionEntry[7].num = numOtherFiles;

      char* mkdir_others[] = { "mkdir", "-p", "categorized/others", NULL };
      bash_command(mkdir_others, "/bin/mkdir");
      write_log("MADE", "categorized/others");
      traverse_directory("files", "categorized/others", "others", "", 0, numOtherFiles);
      ```

  15. Terakhir, untuk jawaban no 4C, kita gunakan bantuan fungsi `customComparator()` untuk men-sorting jumlah file per-extension

      fungsi `customComparator()` :

      ```c
      int customComparator(const void* a, const void* b) {
        ExtensionDetail* ext_count_a = (ExtensionDetail*)a;
        ExtensionDetail* ext_count_b = (ExtensionDetail*)b;
        return ext_count_b->num - ext_count_a->num;
      }
      ```

      ```c
      // ANSWER NUMBER 4C
      qsort(extensionEntry, banyakExtension + 1, sizeof(ExtensionDetail), customComparator);
      for (int i = banyakExtension; i >= 0; i--) {
        printf("%s : %d \n", extensionEntry[i].name, extensionEntry[i].num);
      }
      ```
   
      Result :
      
      directory `categorized/` :
      
      <img src="img/4/4categorized.png" alt="4categorized" width="150" />
      
      `log.txt` :
      
      <img src="img/4/4log.png" alt="4log" width="400" />
      
      `4C` :
      
      <img src="img/4/4categorize.png" alt="4categorize" width="200"/>
   
- ### logchecker.c

   - Untuk menghitung banyaknya ACCESSED yang dilakukan.
      
      ```c
      // GET FREQUENCY OF ACCESSED
      int accessedWordFrequency = getWordFrequency("ACCESSED", fileName);
      printf("ACCESSED FREQUENCY : %d\n", accessedWordFrequency);
      ```
      
      Menggunakan bantuan fungsi `getWordFrequency()` yang didalamnya menggunakan command `awk` untuk emnghitung frekuensi kemunculan dari sebuah kata
   
   - Untuk membuat list seluruh folder yang telah dibuat
   
      ```c
      // GET LIST OF MADE DIRECTORY
      FolderDetail folder[madeWordFrequency - 1];
      getFoldersName(folder, fileName, madeWordFrequency);
      ```
      
      Menggunakan fungsi `getFoldersName()` yang berguna untuk mendapatkan semua baris dari file `log.txt` yang dalam baris tersebut terdapat kata `MADE`. Lalu mengambil string setelah kata `MADE` tersebut yang merepresentasikan nama tiap folder
   
   - Banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
   
      Menggunakan cara yaitu : mentraverse file `log.txt` untuk setiap folder yang dibuat, lalu mengecek perbarisnya apakah ada kata `MOVED` dan nama folder tersebut. Jika panjang nama folder <= 17 (ex: `categorized/jpg`, dll), maka akan dicek juga apakah pada baris tersebut tidak mengandung character `(`
      
      Setelah itu, kita sort secara ASC menggunakan fungsi `customComparator()` :
      
      ```c
      int customComparator(const void* a, const void* b) {
        const FolderDetail* ext_count_a = (const FolderDetail*)a;
        const FolderDetail* ext_count_b = (const FolderDetail*)b;
        return (int)ext_count_b->num - (int)ext_count_a->num;
      }
      ```
      
      ```c
      qsort(folder, madeWordFrequency, sizeof(FolderDetail), customComparator);
      printf("\n------- Total Per Folder -----\n");
      for (int i = madeWordFrequency - 1; i >= 0; i--) {
        printf("%s : %d\n", folder[i].name, folder[i].num);
      }
      ```
   
   - Menghitung banyaknya total file tiap extension, terurut secara ascending.
      
      Jika dalam nama folder tersebut mengandung kata yang sesuai dengan extension masing-masing, maka akan dijumlahkan
   
      ```c
      if (strstr(folder[i].name, "jpg") != NULL) extension[0].num += folder[i].num;
      else if (strstr(folder[i].name, "txt") != NULL) extension[1].num += folder[i].num;
      else if (strstr(folder[i].name, "js") != NULL) extension[2].num += folder[i].num;
      else if (strstr(folder[i].name, "py") != NULL) extension[3].num += folder[i].num;
      else if (strstr(folder[i].name, "png") != NULL) extension[4].num += folder[i].num;
      else if (strstr(folder[i].name, "emc") != NULL) extension[5].num += folder[i].num;
      else if (strstr(folder[i].name, "xyz") != NULL) extension[6].num += folder[i].num;
      else if (strstr(folder[i].name, "others") != NULL) extension[7].num += folder[i].num;
      ```
      
      Lalu hasilnya akan di-sort secara ASC menggunakan fungsi `customComparator()`
      
      ```c
      qsort(extension, banyakExtension, sizeof(FolderDetail), customComparator);
      printf("\n------- Total Per Extension -----\n");
      for (int i = banyakExtension - 1; i >= 0; i--) {
        printf("%s : %d\n", extension[i].name, extension[i].num);
      }
      ```
      
      Result :
      
      `logchecker` :
      
      <img src="img/4/4logchecker.png" alt="4logchecker" width="220" />
